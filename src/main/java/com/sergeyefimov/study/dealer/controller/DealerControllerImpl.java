package com.sergeyefimov.study.dealer.controller;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.dealer.service.DealerService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public final class DealerControllerImpl implements DealerController {
    private final DealerService dealerService;

    @Autowired
    public DealerControllerImpl(DealerService dealerService) {
        this.dealerService = dealerService;
    }

    @Override
    public @NonNull Dealer createDealer() {
        return dealerService.createDealer();
    }
}
