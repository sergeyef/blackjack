package com.sergeyefimov.study.dealer.controller;

import com.sergeyefimov.study.dealer.model.Dealer;
import lombok.NonNull;

public interface DealerController {
    @NonNull
    Dealer createDealer();
}
