package com.sergeyefimov.study.dealer.model;

import com.sergeyefimov.study.card.DeckOfCards;
import com.sergeyefimov.study.card.items.Card;
import lombok.Setter;

public final class Dealer {
    @Setter
    private DeckOfCards deckOfCards;

    public Card getCard() {
        return deckOfCards.getNextCard();
    }
}
