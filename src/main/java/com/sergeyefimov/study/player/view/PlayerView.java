package com.sergeyefimov.study.player.view;

import com.sergeyefimov.study.player.model.Player;
import lombok.NonNull;

public interface PlayerView {
    void printPlayer(@NonNull Player player);
}
