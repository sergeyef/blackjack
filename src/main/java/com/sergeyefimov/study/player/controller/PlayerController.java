package com.sergeyefimov.study.player.controller;

import com.sergeyefimov.study.player.model.Player;
import lombok.NonNull;

import java.math.BigDecimal;

public interface PlayerController {
    @NonNull
    Player createPlayer(@NonNull BigDecimal accountAmount, @NonNull String name);

    boolean doesPlayerHaveMoney(@NonNull Player player, @NonNull BigDecimal amount);
}
