package com.sergeyefimov.study.player.controller;

import com.sergeyefimov.study.player.model.Player;
import com.sergeyefimov.study.player.service.PlayerService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;

@Controller
public final class PlayerControllerImpl implements PlayerController {

    private final PlayerService playerService;

    @Autowired
    public PlayerControllerImpl(PlayerService playerService) {
        this.playerService = playerService;
    }

    @Override
    public @NonNull Player createPlayer(@NonNull BigDecimal accountAmount, @NonNull String name) {
        return playerService.createPlayer(accountAmount, name);
    }

    @Override
    public boolean doesPlayerHaveMoney(@NonNull Player player, @NonNull BigDecimal amount) {
        return playerService.doesPlayerHaveMoney(player, amount);
    }
}
