package com.sergeyefimov.study.player.service;

import com.sergeyefimov.study.player.model.Player;
import lombok.NonNull;

import java.math.BigDecimal;

public interface PlayerService {
    @NonNull
    Player createPlayer(@NonNull BigDecimal accountAmount, @NonNull String name);

    void increaseAccountAmount(@NonNull Player player, @NonNull BigDecimal amount);

    void decreaseAccountAmount(@NonNull Player player, @NonNull BigDecimal amount);

    boolean doesPlayerHaveMoney(@NonNull Player player, @NonNull BigDecimal amount);

}
