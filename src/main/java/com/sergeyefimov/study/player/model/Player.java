package com.sergeyefimov.study.player.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class Player {
    @Setter
    private BigDecimal account;
    private String name;
}
