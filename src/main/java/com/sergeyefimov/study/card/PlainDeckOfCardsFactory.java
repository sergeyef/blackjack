package com.sergeyefimov.study.card;

import org.springframework.stereotype.Component;

@Component
public class PlainDeckOfCardsFactory implements DeckOfCardsFactory{

    @Override
    public DeckOfCards getDeskOfCards() {
        return new PlainDeckOfCards();
    }
}
