package com.sergeyefimov.study.card.items;

import lombok.Getter;

@Getter
public enum CardValue {
    Two(2),
    Three(3),
    Four(4),
    Five(5),
    Six(6),
    Seven(7),
    Eight(8),
    Nine(9),
    Ten(10),
    Jack(10, "J"),
    Queen(10, "Q"),
    King(10, "K"),
    Ace(10, 1, "A");

    private final int hardScore;
    private final int softScore;
    private final String name;

    CardValue(int score, String name) {
        this.hardScore = score;
        this.softScore = score;
        this.name = name;
    }

    CardValue(int score) {
        this.hardScore = score;
        this.softScore = score;
        this.name = String.valueOf(score);
    }

    CardValue(int hardScore, int softScore, String name) {
        this.hardScore = hardScore;
        this.softScore = softScore;
        this.name = name;
    }
}
