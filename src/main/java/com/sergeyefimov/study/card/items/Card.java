package com.sergeyefimov.study.card.items;

import lombok.Value;

@Value
public final class Card {
    private final int hardScore;
    private final int softScore;
    private final String name;
    private final CardValue value;

    public Card(CardSuit suit, CardValue value) {
        this.hardScore = value.getHardScore();
        this.softScore = value.getSoftScore();
        this.value = value;
        this.name = value.getName() + suit.getName();
    }

    @Override
    public String toString() {
        return name;
    }
}
