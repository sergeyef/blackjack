package com.sergeyefimov.study.card.items;

import lombok.Getter;

@Getter
public enum CardSuit {
    Diamonds('\u2666'),
    Spade('\u2660'),
    Club('\u2663'),
    Heart('\u2764');

    private final String name;

    CardSuit(char printableValue) {
        this.name = String.valueOf(printableValue);
    }
}
