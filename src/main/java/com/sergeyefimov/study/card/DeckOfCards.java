package com.sergeyefimov.study.card;

import com.sergeyefimov.study.card.items.Card;

public interface DeckOfCards {
    Card getNextCard();
}
