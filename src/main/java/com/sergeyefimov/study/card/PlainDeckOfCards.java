package com.sergeyefimov.study.card;

import com.sergeyefimov.study.card.items.Card;
import com.sergeyefimov.study.card.items.CardSuit;
import com.sergeyefimov.study.card.items.CardValue;

import java.util.*;

public final class PlainDeckOfCards implements DeckOfCards {
    private static final Set<Card> SET_OF_CARDS;

    // generate set of card
    static {
        List<Card> cards = new ArrayList<>(CardSuit.values().length * CardValue.values().length);
        for (CardSuit suit: CardSuit.values()) {
            for (CardValue value: CardValue.values()) {
                cards.add(new Card(suit, value));
            }

        }
        SET_OF_CARDS = Collections.unmodifiableSet(new HashSet<>(cards));
    }

    private static Deque<Card> generateRandomDesk() {
        List<Card> desk = new ArrayList<>(SET_OF_CARDS);
        Collections.shuffle(desk);
        return new ArrayDeque<>(desk);
    }

    private final Deque<Card> cards = generateRandomDesk();

    @Override
    public Card getNextCard() {
        // cards can't be empty within one round (the game is for single player)
        return cards.pop();
    }
}
