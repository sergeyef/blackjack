package com.sergeyefimov.study.hand.model;

public enum Command {
    HIT, STAND, DOUBLE, SPLIT, SURRENDERED
}
