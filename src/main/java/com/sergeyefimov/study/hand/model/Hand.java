package com.sergeyefimov.study.hand.model;

import com.sergeyefimov.study.card.items.Card;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public final class Hand {
    @Getter
    private final List<Card> cards = new ArrayList<>();

    @Getter
    @Setter
    private State state = State.ACTIVE;

    @Getter
    private BigDecimal bet;

    public Hand(BigDecimal bet) {
        this.bet = bet;
    }

    public void increaseBet(BigDecimal amount) {
        bet = bet.add(amount);
    }
}
