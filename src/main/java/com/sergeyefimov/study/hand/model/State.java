package com.sergeyefimov.study.hand.model;

public enum State {
    ACTIVE(false), SURRENDERED, BUSTED, DONE;

    private final boolean isFinalState;

    State(boolean isFinalState) {
        this.isFinalState = isFinalState;
    }

    State() {
        this.isFinalState = true;
    }

    public boolean canContinue() {
        return !isFinalState;
    }
}
