package com.sergeyefimov.study.hand.service;

import com.sergeyefimov.study.card.items.Card;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.hand.model.Hand;
import com.sergeyefimov.study.hand.model.State;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.Set;

public interface HandService {
    int MAX_SCORE = 21;
    int DOUBLE_LOWER_BOUND = 9; // include
    int DOUBLE_UPPER_BOUND = 12; // exclude

    @NonNull
    Hand createNewHand(@NonNull BigDecimal bet);

    void doubleBet(@NonNull Hand hand);

    void addCard(@NonNull Hand hand, @NonNull Card card);

    int getHardScore(@NonNull Hand hand);

    int getSoftScore(@NonNull Hand hand);

    boolean isBlackJack(@NonNull Hand hand);

    boolean isBusted(@NonNull Hand hand);

    void setState(@NonNull Hand hand, State state);

    int calculateResultScore(@NonNull Hand hand);

    @NonNull
    Set<Command> getAvailableCommands(@NonNull Hand hand);

    boolean isCommandAvailable(@NonNull Hand hand, @NonNull Command command);
}
