package com.sergeyefimov.study.hand.service;

import com.sergeyefimov.study.card.items.Card;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.hand.model.Hand;
import com.sergeyefimov.study.hand.model.State;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static com.sergeyefimov.study.dealer.service.DealerService.INIT_CARD_NUMBER;

@Service
public final class HandServiceImpl implements HandService {
    @Override
    @NonNull
    public Hand createNewHand(@NonNull BigDecimal bet) {
        return new Hand(bet);
    }

    @Override
    public void doubleBet(@NonNull Hand hand) {
        hand.increaseBet(hand.getBet());
    }

    @Override
    public void addCard(@NonNull Hand hand, @NonNull Card card) {
        hand.getCards().add(card);
    }

    @Override
    public int getHardScore(@NonNull Hand hand) {
        return getSumByFunction(Card::getHardScore, hand);
    }

    @Override
    public int getSoftScore(@NonNull Hand hand) {
        return getSumByFunction(Card::getSoftScore, hand);
    }

    private int getSumByFunction(ToIntFunction<Card> function, @NonNull Hand hand) {
        return hand.getCards().stream().mapToInt(function).sum();
    }

    @Override
    public boolean isBlackJack(@NonNull Hand hand) {
        return hand.getCards().size() == INIT_CARD_NUMBER &&
                (getHardScore(hand) == MAX_SCORE || getSoftScore(hand) == MAX_SCORE);
    }

    @Override
    public boolean isBusted(@NonNull Hand hand) {
        return getHardScore(hand) > MAX_SCORE && getSoftScore(hand) > MAX_SCORE;
    }

    @Override
    public void setState(@NonNull Hand hand, State state) {
        hand.setState(state);
    }

    @Override
    public int calculateResultScore(@NonNull Hand hand) {
        int hardScore = getHardScore(hand);
        if (hardScore > MAX_SCORE) {
            return getSoftScore(hand);
        } else {
            return hardScore;
        }
    }

    @Override
    public @NonNull Set<Command> getAvailableCommands(@NonNull Hand hand) {
        return Arrays.stream(Command.values())
                .filter(c->isCommandAvailable(hand, c))
                .collect(Collectors.toSet());
    }

    @Override
    public boolean isCommandAvailable(@NonNull Hand hand, @NonNull Command command) {
        boolean result;
        switch (command) {
            case HIT:
                result = isHitAvailable(hand);
                break;
            case SPLIT:
                result = isSplitAvailable(hand);
                break;
            case STAND:
                result = isStandAvailable(hand);
                break;
            case DOUBLE:
                result = isDoubleAvailable(hand);
                break;
            case SURRENDERED:
                result = isSurrenderedAvailable(hand);
                break;
            default:
                result = false;
        }
        return result;
    }

    private boolean isHitAvailable(@NonNull Hand hand) {
        return hand.getState() == State.ACTIVE;
    }

    private boolean isStandAvailable(@NonNull Hand hand) {
        return hand.getState() == State.ACTIVE;
    }

    private boolean isDoubleAvailable(@NonNull Hand hand) {
        return hand.getCards().size() <= INIT_CARD_NUMBER &&
                ((getHardScore(hand) >= DOUBLE_LOWER_BOUND && getHardScore(hand) < DOUBLE_UPPER_BOUND) ||
                (getSoftScore(hand) >= DOUBLE_LOWER_BOUND && getSoftScore(hand) < DOUBLE_UPPER_BOUND));
    }

    private boolean isSplitAvailable(@NonNull Hand hand) {
        return hand.getCards().size() > 1 &&
                hand.getCards().stream().map(Card::getValue).distinct().limit(2).count() <= 1;
    }

    private boolean isSurrenderedAvailable(@NonNull Hand hand) {
        return hand.getState() == State.ACTIVE;
    }
}
