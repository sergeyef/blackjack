package com.sergeyefimov.study.hand.view;

import com.sergeyefimov.study.card.items.Card;
import com.sergeyefimov.study.hand.model.Hand;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.stream.Collectors;

@Component("plainHandView")
public final class plainHandViewImpl implements HandView {
    @Override
    public void printHand(@NonNull Hand hand) {
        if(!hand.getBet().equals(BigDecimal.ZERO)) {
            System.out.println("Bet: " + hand.getBet());
        }
        System.out.println("Cards: " + hand.getCards().stream().map(Card::toString).collect(Collectors.joining(" | ")));
    }
}
