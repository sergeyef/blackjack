package com.sergeyefimov.study.hand.view;

import com.sergeyefimov.study.hand.model.Hand;
import lombok.NonNull;

public interface HandView {
    void printHand(@NonNull Hand hand);
}
