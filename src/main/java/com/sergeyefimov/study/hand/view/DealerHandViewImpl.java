package com.sergeyefimov.study.hand.view;

import com.sergeyefimov.study.card.items.Card;
import com.sergeyefimov.study.hand.model.Hand;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component("dealerHandView")
public final class DealerHandViewImpl implements HandView {

    @Override
    public void printHand(@NonNull Hand hand) {
        System.out.println("Cards: xx "
                + hand.getCards().subList(1, hand.getCards().size())
                .stream().map(Card::toString).collect(Collectors.joining(" | ")));
    }
}
