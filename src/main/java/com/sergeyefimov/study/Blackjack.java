package com.sergeyefimov.study;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.dealer.controller.DealerController;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.hand.model.Hand;
import com.sergeyefimov.study.hand.view.HandView;
import com.sergeyefimov.study.player.model.Player;
import com.sergeyefimov.study.player.controller.PlayerController;
import com.sergeyefimov.study.player.view.PlayerView;
import com.sergeyefimov.study.round.model.Round;
import com.sergeyefimov.study.round.controller.RoundController;
import com.sergeyefimov.study.round.model.RoundResult;
import com.sergeyefimov.study.round.view.RoundResultView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@SpringBootApplication
public class Blackjack implements CommandLineRunner
{
    private static final BigDecimal START_AMOUNT = new BigDecimal("100");
    private static final BigDecimal ROUND_AMOUNT = new BigDecimal("10");

    private final PlayerController playerController;
    private final DealerController dealerController;
    private final RoundController roundController;
    private final HandView dealerHandView;
    private final HandView plainHandView;
    private final PlayerView playerView;
    private final RoundResultView roundResultView;

    @Autowired
    public Blackjack(PlayerController playerController,
                     DealerController dealerController,
                     RoundController roundController,
                     @Qualifier("dealerHandView") HandView dealerHandView,
                     @Qualifier("plainHandView") HandView plainHandView,
                     PlayerView playerView,
                     RoundResultView roundResultView) {
        this.playerController = playerController;
        this.dealerController = dealerController;
        this.roundController = roundController;
        this.dealerHandView = dealerHandView;
        this.plainHandView = plainHandView;
        this.playerView = playerView;
        this.roundResultView = roundResultView;
    }

    public static void main(String[] args ) {
        SpringApplication.run(Blackjack.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        do {
            playGame(scanner);
            System.out.println("Would you like to start a new game? (y/n)");
        } while(scanner.nextLine().toLowerCase().equals("y"));

    }

    private void playGame(Scanner scanner) throws IOException {
        // create a Player
        Player player = playerController.createPlayer(START_AMOUNT, "Sergey");
        // create a Dealer
        Dealer dealer = dealerController.createDealer();

        // while the Player has enough money do
        while (playerController.doesPlayerHaveMoney(player, ROUND_AMOUNT)) {
            // create new round
            Round round = roundController.createRound(player, dealer, ROUND_AMOUNT);

            printPlayerHands(round.getPlayer(), round.getPlayerHands());
            printDealerHand(round.getDealerHand(), false);

            // while round is not finished
            while (!roundController.isRoundOver(round)) {
                // for each hand
                for (int i = 0; i < round.getPlayerHands().size(); i++) {
                    // get possible commands
                    Set<Command> commands = roundController.getAvailableCommandsList(round, i);

                    if (!commands.isEmpty()) {
                        System.out.println("Hand " + i + ". Choice action:");
                        Command command = askCommands(scanner, commands);
                        // execute command
                        roundController.executeCommand(round, i, command);
                    }

                    printPlayerHands(round.getPlayer(), round.getPlayerHands());
                }

            }
            // finish round
            roundController.finishRound(round);

            printDealerHand(round.getDealerHand(), true);

            // for each hand get results
            for (int i = 0; i < round.getPlayerHands().size(); i++) {
                RoundResult result = roundController.getHandResult(round, i);
                System.out.print("Hand " + i + ": ");
                roundResultView.printRoundResult(result);
            }

            playerView.printPlayer(player);

            // do you want to continue?
            System.out.println("Continue this game (y/n)");
            if (!scanner.nextLine().toLowerCase().equals("y")) {
                break;
            }
        }
    }

    private void printPlayerHands(Player player, List<Hand> hands) {
        playerView.printPlayer(player);
        for (int i = 0; i < hands.size(); i++) {
            System.out.println("-- Hand " + i + ":");
            plainHandView.printHand(hands.get(i));
        }
    }

    private void printDealerHand(Hand hand, boolean isFirstCardOpen) {
        System.out.println("- Dealer: ");
        HandView viewer = isFirstCardOpen ? plainHandView : dealerHandView;
        viewer.printHand(hand);
    }

    private Command askCommands(Scanner scanner, Set<Command> commands) {
        List<Command> commandsList = new ArrayList<>(commands);
        for (int i = 0; i < commandsList.size(); i++) {
            System.out.println((i + 1) + ". " + commandsList.get(i));
        }
        return commandsList.get(Integer.valueOf(scanner.nextLine().trim()) - 1);
    }

}
