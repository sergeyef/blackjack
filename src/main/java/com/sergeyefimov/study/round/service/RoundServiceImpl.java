package com.sergeyefimov.study.round.service;

import com.sergeyefimov.study.card.items.Card;
import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.dealer.service.DealerService;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.hand.model.Hand;
import com.sergeyefimov.study.hand.service.HandService;
import com.sergeyefimov.study.player.model.Player;
import com.sergeyefimov.study.player.service.PlayerService;
import com.sergeyefimov.study.round.model.Round;
import com.sergeyefimov.study.round.model.RoundResult;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.sergeyefimov.study.hand.model.Command.DOUBLE;
import static com.sergeyefimov.study.hand.model.Command.SPLIT;
import static com.sergeyefimov.study.hand.model.State.*;
import static com.sergeyefimov.study.round.model.RoundResult.*;

@Service
public final class RoundServiceImpl implements RoundService {

    private final DealerService dealerService;
    private final HandService handService;
    private final PlayerService playerService;

    @Autowired
    public RoundServiceImpl(DealerService dealerService, HandService handService, PlayerService playerService) {
        this.dealerService = dealerService;
        this.handService = handService;
        this.playerService = playerService;
    }

    @Override
    @NonNull
    public Round createRound(@NotNull Player player, @NotNull Dealer dealer, @NonNull BigDecimal bet) {
        dealerService.initialize(dealer);
        Hand playerHand = handService.createNewHand(bet);
        Hand dealerHand = handService.createNewHand(BigDecimal.ZERO);
        dealerService.cardDistribution(dealer, playerHand);
        dealerService.cardDistribution(dealer, dealerHand);

        // check if dealer or player have blackjack, in that case round is over,
        // to mark it - lets set hand state to DONE
        if (handService.isBlackJack(playerHand) || handService.isBlackJack(dealerHand)) {
            handService.setState(playerHand, DONE);
        }

        // decrease Player account amount
        if (!playerService.doesPlayerHaveMoney(player, bet)) {
            // shouldn't happen
            throw new RuntimeException("Player doesn't have enough money to start round. " +
                    "Account should be equal or greater than " + bet);
        }
        playerService.decreaseAccountAmount(player, bet);
        List<Hand> handList = new ArrayList<>();
        handList.add(playerHand);

        return new Round(handList, dealerHand, dealer, player);
    }

    @Override
    @NonNull
    public Set<Command> getAvailableCommands(@NonNull Round round, int handIndex) {
        if (round.getPlayerHands().size() <= handIndex) {
            throw new RuntimeException("Invalid hand index");
        }

        Hand hand = round.getPlayerHands().get(handIndex);

        if (hand.getState().canContinue()) {
            return handService.getAvailableCommands(round.getPlayerHands().get(handIndex));
        } else {
            return Collections.emptySet();
        }

    }

    @Override
    public void executeCommand(@NonNull Round round, int handIndex, @NonNull Command command) {
        if (round.getPlayerHands().size() <= handIndex) {
            throw new RuntimeException("Invalid hand index");
        }

        Hand hand = round.getPlayerHands().get(handIndex);
        Player player = round.getPlayer();
        Dealer dealer = round.getDealer();

        if (!handService.isCommandAvailable(hand, command)) {
            throw new RuntimeException("Command " + command + " is not available for current hand state");
        }

        if (command == DOUBLE || command == SPLIT) {
            // check that player has enough money
            if (!playerService.doesPlayerHaveMoney(player, hand.getBet())) {
                throw new RuntimeException("Player doesn't have enough money to execute this command. " +
                        "Account should be equal or greater than " + hand.getBet());
            }
            // decrease amount
            playerService.decreaseAccountAmount(player, hand.getBet());
        }

        switch (command) {
            case HIT:
                executeHitCommand(hand, dealer);
                break;
            case DOUBLE:
                executeDoubleCommand(hand, dealer);
                break;
            case SPLIT:
                executeSplitCommand(hand, round);
                break;
            case SURRENDERED:
                executeSurrenderedCommand(hand);
                break;
            case STAND:
                executeStandCommand(hand);
                break;
        }
    }

    @Override
    public boolean isRoundOver(@NonNull Round round) {
        return round.getPlayerHands().stream().noneMatch(p -> p.getState().canContinue());
    }

    @Override
    public void finishRound(@NonNull Round round) {
        dealerService.dealToYourself(round.getDealer(), round.getDealerHand());
    }

    @Override
    @NonNull
    public RoundResult getHandResult(@NonNull Round round, int handIndex) {
        if (round.getPlayerHands().size() <= handIndex) {
            throw new RuntimeException("Invalid hand index");
        }
        Hand hand = round.getPlayerHands().get(handIndex);
        switch (hand.getState()) {
            case DONE:
                return getDoneHandResult(round, hand);
            case SURRENDERED:
                return getSurrenderedHandResult(round, hand);
            case BUSTED:
                return getBustedHandResult(round, hand);
            default:
                 throw new RuntimeException("Hand has invalid state " + hand.getState());
        }
    }

    private RoundResult getDoneHandResult(@NonNull Round round, @NonNull Hand hand) {
        boolean dealerHasBlackjack = handService.isBlackJack(round.getDealerHand());
        RoundResult result;
        BigDecimal playerWon;
        if (handService.isBlackJack(hand)) {
            if (dealerHasBlackjack) {
                playerWon = hand.getBet();
                result = DRAW;
            } else {
                playerWon = hand.getBet().multiply(new BigDecimal("2"));
                result = HAND_WON;
            }
        } else {
            int dealerScore = handService.calculateResultScore(round.getDealerHand());
            int handScore = handService.calculateResultScore(hand);
            if (dealerHasBlackjack) {
                playerWon = BigDecimal.ZERO;
                result = DEALER_WON;
            }
            else if ( round.getDealerHand().getState() == BUSTED || handScore > dealerScore ) {
                playerWon = hand.getBet().multiply(new BigDecimal("2"));
                result = HAND_WON;
            } else if(dealerScore == handScore) {
                playerWon = hand.getBet();
                result = DRAW;
            } else {
                playerWon = BigDecimal.ZERO;
                result = DEALER_WON;
            }
        }

        playerService.increaseAccountAmount(round.getPlayer(), playerWon);
        return result;
    }

    private RoundResult getSurrenderedHandResult(@NonNull Round round, @NonNull Hand hand) {
        playerService.increaseAccountAmount(round.getPlayer(), hand.getBet().divide(new BigDecimal("2"), RoundingMode.DOWN));
        return DEALER_WON;
    }

    private RoundResult getBustedHandResult(@NonNull Round round, @NonNull Hand hand) {
        if (round.getDealerHand().getState() == BUSTED) {
            playerService.increaseAccountAmount(round.getPlayer(), hand.getBet());
            return DRAW;
        } else {
            return DEALER_WON;
        }
    }

    private void executeHitCommand(@NonNull Hand hand, @NonNull Dealer dealer) {
        dealerService.addCardToHand(dealer, hand);
        if (handService.isBusted(hand)) {
            handService.setState(hand, BUSTED);
        }
    }

    private void executeStandCommand(@NonNull Hand hand) {
        handService.setState(hand, DONE);
    }

    private void executeDoubleCommand(@NonNull Hand hand, @NonNull Dealer dealer) {
        dealerService.addCardToHand(dealer, hand);
        if (handService.isBusted(hand)) {
            handService.setState(hand, BUSTED);
        } else {
            handService.doubleBet(hand);
            handService.setState(hand, DONE);
        }
    }

    private void executeSplitCommand(@NonNull Hand hand, @NonNull Round round) {
        List<Hand> roundHands = round.getPlayerHands();
        roundHands.remove(hand);
        for (Card card : hand.getCards()) {
            Hand newHand = handService.createNewHand(hand.getBet());
            handService.addCard(newHand, card);
            roundHands.add(newHand);
        }
    }

    private void executeSurrenderedCommand(@NonNull Hand hand) {
        handService.setState(hand, SURRENDERED);
    }

}
