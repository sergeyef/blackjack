package com.sergeyefimov.study.round.service;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.player.model.Player;
import com.sergeyefimov.study.round.model.Round;
import com.sergeyefimov.study.round.model.RoundResult;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import java.math.BigDecimal;
import java.util.Set;

public interface RoundService {
    @NonNull
    Round createRound(@NotNull Player player, @NotNull Dealer dealer, @NonNull BigDecimal bet);

    boolean isRoundOver(@NonNull Round round);

    void finishRound(@NonNull Round round);

    @NonNull
    Set<Command> getAvailableCommands(@NonNull Round round, int handIndex);

    void executeCommand(@NonNull Round round, int handIndex, @NonNull Command command);

    @NonNull
    RoundResult getHandResult(@NonNull Round round, int handIndex);
}
