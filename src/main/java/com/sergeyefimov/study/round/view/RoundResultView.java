package com.sergeyefimov.study.round.view;

import com.sergeyefimov.study.round.model.RoundResult;
import lombok.NonNull;

public interface RoundResultView {
    void printRoundResult(@NonNull RoundResult roundResult);
}
