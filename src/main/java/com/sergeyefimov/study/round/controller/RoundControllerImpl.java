package com.sergeyefimov.study.round.controller;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.player.model.Player;
import com.sergeyefimov.study.round.model.RoundResult;
import com.sergeyefimov.study.round.service.RoundService;
import com.sergeyefimov.study.round.model.Round;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.util.Set;

@Controller
public final class RoundControllerImpl implements RoundController {

    private final RoundService roundService;

    @Autowired
    public RoundControllerImpl(RoundService roundService) {
        this.roundService = roundService;
    }

    @Override
    @NonNull
    public Round createRound(@NotNull Player player, @NotNull Dealer dealer, @NonNull BigDecimal bet) {
        return roundService.createRound(player, dealer, bet);
    }

    @Override
    @NotNull
    public Set<Command> getAvailableCommandsList(@NonNull Round round, int handIndex) {
        return roundService.getAvailableCommands(round, handIndex);
    }

    @Override
    public void executeCommand(@NonNull Round round, int handIndex, @NonNull Command command) {
        roundService.executeCommand(round, handIndex, command);
    }

    @Override
    public boolean isRoundOver(@NonNull Round round) {
        return roundService.isRoundOver(round);
    }

    @Override
    public void finishRound(@NonNull Round round) {
        roundService.finishRound(round);
    }

    @Override
    public @NonNull RoundResult getHandResult(@NonNull Round round, int handIndex) {
        return roundService.getHandResult(round, handIndex);
    }
}
