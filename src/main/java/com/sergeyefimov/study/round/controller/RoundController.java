package com.sergeyefimov.study.round.controller;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.player.model.Player;
import com.sergeyefimov.study.round.model.RoundResult;
import com.sergeyefimov.study.round.model.Round;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.Set;

public interface RoundController {
    @NonNull
    Round createRound(@NotNull Player player, @NotNull Dealer dealer, @NonNull BigDecimal bet);

    @NotNull
    Set<Command> getAvailableCommandsList(@NonNull Round round, int handIndex);

    @NonNull
    void executeCommand(@NonNull Round round, int handIndex, @NonNull Command command);

    boolean isRoundOver(@NonNull Round round);

    void finishRound(@NonNull Round round);

    @NonNull
    RoundResult getHandResult(@NonNull Round round, int handIndex);
}
