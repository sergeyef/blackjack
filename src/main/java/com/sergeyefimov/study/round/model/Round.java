package com.sergeyefimov.study.round.model;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.hand.model.Hand;
import com.sergeyefimov.study.player.model.Player;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.List;

@Getter
public class Round {
    @Setter
    private List<Hand> playerHands;
    private Hand dealerHand;
    private Dealer dealer;
    private Player player;
    @Setter
    private boolean isRoundOver = false;

    public Round(@NonNull List<Hand> playerHands, @NonNull Hand dealerHand, @NonNull Dealer dealer, @NonNull Player player) {
        this.playerHands = playerHands;
        this.dealerHand = dealerHand;
        this.dealer = dealer;
        this.player = player;
    }
}
