package com.sergeyefimov.study.round.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum RoundResult {
    DRAW("Draw"),
    HAND_WON("The hand won"),
    DEALER_WON("Dealer won");

    @Getter
    private final String description;
}
