# blackjack

Simple BlackJack CLI implementation.
* *Blackjack.java* - main class (some sort of frontend emulation)
* *src/** - backend

Implemented using Spring Boot, not necessary for this game but allows to migrate it to web implementation.
To do this just rewrite controllers with Spring Web and enjoy.